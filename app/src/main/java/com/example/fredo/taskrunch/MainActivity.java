package com.example.fredo.taskrunch;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import com.example.fredo.taskrunch.adapter.TabsAdapter;
import com.example.fredo.taskrunch.model.Task;
import com.example.fredo.taskrunch.model.TaskStorage;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity implements TabFragmentListener {


    private static final String TAG = "ADDING_DUE_DATE";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private TabsAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TaskStorage storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs_activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new TabsAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        RealmConfiguration realmConfig = new  RealmConfiguration.Builder(this)
                .name("RealmWithSubtasks")
                .build();
        Realm.setDefaultConfiguration(realmConfig);
        storage = new TaskStorage(Realm.getDefaultInstance());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        storage.close();
        super.onDestroy();
    }

    @Override
    public void clear_completeBtnClicked() {
        //
    }

    private void refreshTab() {
        //this doesn't do anything because the fragments data don't change in any way, and the
        //mSectionsPagerAdapter doesn't hold any references to anything that can even change
        //mSectionsPagerAdapter.notifyDataSetChanged();
        /*
        Here you can either use an interface or use the .onResume() method of the fragment, either
        way the fragment is already created and ready to go because instantiateItem() sets it up!!!
        A interface/callback set up is a lot more clear than piggyback riding in the forced to be called
        onResume() method because onResume() isn't called automatically, only when you force it,
        because the parent activity is never onResumed() and fragments only
        onResume() when their parent activity is onResume()'ed! so you shouldn't actually override
        these methods in the fragment for this single activity project
        AllTasksTabFragment allTasksFragment =
                (AllTasksTabFragment) mSectionsPagerAdapter.instantiateItem(mViewPager,TabsAdapter.ALL_POS);
        allTasksFragment.onResume();
        */
        TabsActivityListener listener =
                (TabsActivityListener) mSectionsPagerAdapter.instantiateItem(mViewPager,TabsAdapter.ALL_POS);
        listener.updateAllTasks();
    }

    @Override
    public void add_taskBtnClicked() {
        Log.d(TAG, "add_taskBtnClicked() called with: " + "");
        String title = ((EditText) findViewById(R.id.taskTitle)).getText().toString();
        String dueDateStr = ((EditText) findViewById(R.id.dueDate)).getText().toString();
        String description = ((EditText) findViewById(R.id.description)).getText().toString();
        String lengthStr = ((EditText) findViewById(R.id.length)).getText().toString();
        float length = Float.parseFloat(lengthStr);

        DateFormat regDateTimeFormat = new SimpleDateFormat("mm/dd/yy HH:mm");
        Task task = new Task();
        try {
            Date dueDate = (Date)regDateTimeFormat.parse(dueDateStr);
            task.setDueDate(dueDate);
        } catch (ParseException e) {
            Log.e(TAG, "onAddTaskButtonPress: format incorrect for date");
            e.printStackTrace();
        }
        task.setLength(length);
        task.setTitle(title);
        task.setDateAdded(new Date());
        storage.saveTask(task);
        refreshTab();
    }
}
