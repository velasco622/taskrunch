package com.example.fredo.taskrunch;

/**
 * Created by alfredovelasco on 9/8/16.
 * The Activity uses this interface to listen to the fragments
 */
public interface TabFragmentListener {
    void add_taskBtnClicked();
    void clear_completeBtnClicked();
}
