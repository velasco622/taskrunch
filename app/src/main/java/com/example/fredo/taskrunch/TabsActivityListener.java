package com.example.fredo.taskrunch;

/**
 * Created by Fredo on 9/29/16.
 * The fragments use this listener to listen to the activity
 */
public interface TabsActivityListener {
    void updateAllTasks();
}
