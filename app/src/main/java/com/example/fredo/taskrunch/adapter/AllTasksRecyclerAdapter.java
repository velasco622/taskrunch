package com.example.fredo.taskrunch.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.example.fredo.taskrunch.R;
import com.example.fredo.taskrunch.model.Task;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by Fredo on 8/29/16.
 */
public class AllTasksRecyclerAdapter extends RecyclerView.Adapter<AllTasksRecyclerAdapter.TaskViewHolder>{
    private static final String TAG = "AllTasksRecyclerAdapter";

    private RealmResults<Task> taskList;

    public AllTasksRecyclerAdapter(RealmResults<Task> taskList){
        Log.d(TAG, "AllTasksRecyclerAdapter() called with: " + "taskList  size of = [" + taskList.size() + "]");

        this.taskList=taskList;
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder() called with: " + "holder = [" + holder + "], position = [" + position + "]");
        Task task = taskList.get(position);
        holder.vCompleteBox.setChecked(task.isComplete());
        holder.vCompleteBox.setText(task.getTitle());
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.simple_taskcard, parent, false);
        return new TaskViewHolder(itemView);
    }

    public static class TaskViewHolder extends RecyclerView.ViewHolder{
        protected CheckBox vCompleteBox;

        public TaskViewHolder(View v){
            super(v);
            vCompleteBox = (CheckBox) v.findViewById(R.id.completionCheckBox);
        }
    }
}
