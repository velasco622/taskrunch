package com.example.fredo.taskrunch.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.example.fredo.taskrunch.R;
import com.example.fredo.taskrunch.model.Task;

import java.util.List;

/**
 * Created by Fredo on 8/29/16.
 */
public class SimpleTaskRecyclerAdapter extends RecyclerView.Adapter<SimpleTaskRecyclerAdapter.TaskViewHolder>{

    private List<Task> taskList;

    public SimpleTaskRecyclerAdapter(List<Task> taskList){
        this.taskList=taskList;
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        Task task = taskList.get(position);
        holder.vCompleteBox.setChecked(task.isComplete());
        holder.vCompleteBox.setText(task.getTitle());
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.simple_taskcard, parent, false);
        return new TaskViewHolder(itemView);
    }

    public static class TaskViewHolder extends RecyclerView.ViewHolder{
        protected CheckBox vCompleteBox;

        public TaskViewHolder(View v){
            super(v);
            vCompleteBox = (CheckBox) v.findViewById(R.id.completionCheckBox);
        }
    }
}
