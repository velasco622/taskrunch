package com.example.fredo.taskrunch.adapter;

/**
 * Created by Fredo on 8/29/16.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.fredo.taskrunch.fragment.AddTaskFragment;
import com.example.fredo.taskrunch.fragment.AllTasksTabFragment;
import com.example.fredo.taskrunch.fragment.PlaceholderFragment;
import com.example.fredo.taskrunch.fragment.TodayTaskFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class TabsAdapter extends FragmentPagerAdapter {

    public static final int TODAY_POS = 0;
    public static final int ALL_POS = 1;
    public static final int ADD_POS = 2;
    public static final int SPLIT_POS = 3;
    public static final int NUM_TABS = 4;

    public TabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a com.example.fredo.taskrunch.fragment.PlaceholderFragment (defined as a static inner class below).
        switch(position){
            case TODAY_POS:
                return TodayTaskFragment.newInstance(position + 1);
            case ALL_POS:
                return AllTasksTabFragment.newInstance(position + 1);
            case ADD_POS:
                return AddTaskFragment.newInstance(position + 1);
            case SPLIT_POS:
                return PlaceholderFragment.newInstance(position + 1);
            default:
                return PlaceholderFragment.newInstance(position + 1);
        }
    }

    @Override
    public int getCount() {
        // Show 4 total pages.
        return NUM_TABS;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case TODAY_POS:
                return "TODAY";
            case ALL_POS:
                return "ALL";
            case ADD_POS:
                return "ADD";
            case SPLIT_POS:
                return "SPLIT";
        }
        return null;
    }

}