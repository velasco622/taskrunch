package com.example.fredo.taskrunch.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.example.fredo.taskrunch.TabFragmentListener;
import com.example.fredo.taskrunch.R;

/**
 * Created by Fredo on 8/29/16.
 */

/**
 * A placeholder fragment containing a simple view.
 */
public class AddTaskFragment extends Fragment {
    public static final String ARG_SECTION_NUMBER = "section_number";
    private TabFragmentListener listener;

    public AddTaskFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AddTaskFragment newInstance(int sectionNumber) {
        AddTaskFragment fragment = new AddTaskFragment();
        Bundle args = new Bundle();
        args.putInt(AddTaskFragment.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    //onAttach(context) is called when the fragment is attached to its parent activity, and
    //the context in the argument belongs to this parent activity!!!
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (TabFragmentListener)context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_task, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.add_task_section_label);
        Button addTaskBtn = (Button) rootView.findViewById(R.id.addTaskBtn);
        addTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.add_taskBtnClicked();
            }
        });
        return rootView;
    }


}