package com.example.fredo.taskrunch.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.fredo.taskrunch.R;
import com.example.fredo.taskrunch.TabFragmentListener;
import com.example.fredo.taskrunch.TabsActivityListener;
import com.example.fredo.taskrunch.adapter.AllTasksRecyclerAdapter;
import com.example.fredo.taskrunch.model.Task;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Fredo on 8/29/16.
 */

/**
 * A placeholder fragment containing a simple view.
 */
public class AllTasksTabFragment extends Fragment implements TabsActivityListener {
    private static final String TAG = "AllTasksTabFragment";
    public static final String ARG_SECTION_NUMBER = "section_number";
    private AllTasksRecyclerAdapter recAdapter;
    private View rootView;
    private RecyclerView recList;
    RealmResults<Task> tasks;
    private TabFragmentListener activitysEear;

    public AllTasksTabFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AllTasksTabFragment newInstance(int sectionNumber) {
        AllTasksTabFragment fragment = new AllTasksTabFragment();
        Bundle args = new Bundle();
        args.putInt(AllTasksTabFragment.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    //onAttach(context) is called when the fragment is attached to its parent activity, and
    //the context in the argument belongs to this parent activity!!!
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activitysEear = (TabFragmentListener)context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_today_and_all_tabs, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText(getString(R.string.section_format, getArguments().getInt(AllTasksTabFragment.ARG_SECTION_NUMBER)));

        Button clearCheckedBtn = (Button)rootView.findViewById(R.id.allTabClearCompletedBtn);
        clearCheckedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateAllTasksList();
            }
        });

        recList = (RecyclerView) rootView.findViewById(R.id.tasksList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated() called with: " + "view = [" + view + "], savedInstanceState = [" + savedInstanceState + "]");
        setUpAdapter();
    }


    /*
    This method is only called when the parent activity that contains the fragment is onResume()'ed
    which is our case–because we only have one activity–never happens!
    @Override
    public void onResume() {
        super.onResume();
//        updateAllTasksList();
    }
    */
//    only called when the activity is created and the fragment is attached to the activity!//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        updateAllTasksList();
//    }

    public void setUpAdapter() {
        Log.d(TAG, "setUpAdapter() called with: " + "");
        tasks = Realm.getDefaultInstance()
                .where(Task.class)
                .equalTo("isComplete", false)
                .findAll();
        recAdapter = new AllTasksRecyclerAdapter(tasks);
        if (recList != null) {
            recList.setAdapter(recAdapter);
        }
    }


    @Override
    public void updateAllTasks() {
        updateAllTasksList();
    }

    public void updateAllTasksList() {
        if(recAdapter!=null)
            recAdapter.notifyDataSetChanged();
    }
}