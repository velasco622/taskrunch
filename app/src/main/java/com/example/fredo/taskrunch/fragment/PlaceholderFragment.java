package com.example.fredo.taskrunch.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fredo.taskrunch.R;
import com.example.fredo.taskrunch.adapter.SimpleTaskRecyclerAdapter;
import com.example.fredo.taskrunch.model.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fredo on 8/29/16.
 */

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {
        public static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
                PlaceholderFragment fragment = new PlaceholderFragment();
                Bundle args = new Bundle();
                args.putInt(PlaceholderFragment.ARG_SECTION_NUMBER, sectionNumber);
                fragment.setArguments(args);
                return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
                View rootView = inflater.inflate(R.layout.fragment_main, container, false);
                TextView textView = (TextView) rootView.findViewById(R.id.section_label);
                textView.setText(getString(R.string.section_format, getArguments().getInt(PlaceholderFragment.ARG_SECTION_NUMBER)));

                RecyclerView recList = (RecyclerView) rootView.findViewById(R.id.cardList);
                recList.setHasFixedSize(true);
                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recList.setLayoutManager(llm);
                Task task1 = new Task("Task1");
                Task task2 = new Task("Task2");
                List<Task> taskList = new ArrayList<Task>();
                taskList.add(task1);
                taskList.add(task2);
                SimpleTaskRecyclerAdapter recAdapter = new SimpleTaskRecyclerAdapter(taskList);
                recList.setAdapter(recAdapter);
                return rootView;
        }
}