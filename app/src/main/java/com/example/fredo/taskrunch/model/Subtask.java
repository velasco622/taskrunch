package com.example.fredo.taskrunch.model;

import io.realm.RealmObject;

/**
 * Created by Fredo on 9/5/16.
 */
public class Subtask extends RealmObject {
    private String superTask;
    private float length;
    private String description;

    public String getSuperTask() {
        return superTask;
    }

    public void setSuperTask(String superTask) {
        this.superTask = superTask;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
