package com.example.fredo.taskrunch.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Fredo on 8/29/16.
 */
public class Task extends RealmObject {

    @PrimaryKey
    private String title = "";
    private String description = "";
    private float length = 0; //in hours
    private Date dueDate = null;
    private Date dateAdded = null;
    private boolean isComplete = false;
    private RealmList<Subtask> subtasks;

    public Task(){}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmList<Subtask> getSubtasks() {
        return subtasks;
    }

    public void setSubtasks(RealmList<Subtask> subtasks) {
        this.subtasks = subtasks;
    }

    public Task(String title){
        this.title = title;
    }

    public float getLength() {
        return length;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getTitle() {
        return title;
    }
}
