package com.example.fredo.taskrunch.model;

import com.example.fredo.taskrunch.model.Task;

import io.realm.Realm;
import io.realm.internal.Context;

/**
 * Created by Fredo on 9/1/16.
 */
public class TaskStorage {
    private Realm theTaskRealm;

    public TaskStorage(Realm theTaskRealm){
        this.theTaskRealm = theTaskRealm;
    }

    public void close() {
        theTaskRealm.close();
    }

    public void saveTask(Task task) {
        theTaskRealm.beginTransaction();
        //Add try and catch here for primary key already existing
        theTaskRealm.copyToRealm(task);
        theTaskRealm.commitTransaction();
    }
}
